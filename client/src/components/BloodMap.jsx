import React, { Component } from 'react';
import { Map, Graphic, Symbols, Geometry } from 'react-arcgis';
import NewPointModal from './NewPointModal';
import './BloodMap.css';


const Point = (props) => (
  <Graphic key={props.latitude}>
    <Symbols.SimpleMarkerSymbol
      symbolProperties={{
        color: [226, 119, 40],
        outline: {
          color: [255, 255, 255],
          width: 2
        }
      }}
      />
    <Geometry.Point
      geometryProperties={{
        latitude: props.latitude,
        longitude: props.longitude
      }}
      />
  </Graphic>
);

class BloodMap extends Component {

  constructor(props){
    super(props);

    this.onLoad = this.onLoad.bind(this);
  }

  onLoad(map, view) {
    setTimeout(() => this.props.onLoadingFinished(), 1000);

    view.on("click", (evt) => {
      let location = {
        latitude: evt.mapPoint.latitude,
        longitude: evt.mapPoint.longitude
      }
      const screenPoint = {
        x: evt.x,
        y: evt.y
      };

      view.hitTest(screenPoint)
            .then((response) => {
              if(response.results.length > 0) {
                const graphic = response.results[0].graphic;
                location = {
                  latitude: graphic.geometry.latitude,
                  longitude: graphic.geometry.longitude,
                }
                this.props.openModalReadOnly(location);
              } else {
                this.props.openModal(location);
                console.log('nao cliquei em ponto');
              }
            });
    });


    let oldExtent = null;
    setInterval( () => {

      if(!oldExtent || (view.extent.xmax !== oldExtent.xmax) ){
        oldExtent = view.extent;
        this.props.onViewUpdate();
      }
    }, 1000);

  }

  render() {
  const p = this.props.points.map((po) => Point({latitude:po.latitude, longitude:po.longitude}));

  return (
      <div className="App">
      <section className="hero is-dark has-text-centered">
      <div className="hero-body">
      <div className="container">
      <h1 className="title">
      Blood Donation
    </h1>
      </div>
      </div>
      </section>
    <div style={{ width: '100vw', height: '80vh' }}>
      <Map
        viewProperties={{center: this.props.actualLocation }}
        onDoubleClick={(evt) => { console.log(this);}}
        onLoad={this.onLoad}
      >
        {p}
      </Map>
      </div>
        {<NewPointModal isOpen={this.props.isModalOpened} closeModal={this.props.closeModal} sendNewPoint={this.props.sendNewPoint} latitude={this.props.modalLatitude} longitude={this.props.modalLongitude} point={this.props.modalPoint} isModalReadOnly={this.props.isModalReadOnly}/>}
      </div>
  );
  };
}

export default BloodMap;
