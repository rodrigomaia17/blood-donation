import React, { Component } from 'react';
import classNames from 'classnames';
import Formsy, {HOC} from 'formsy-react';
import axios from 'axios';

const PointModal = (props) => {
  return (<div className={classNames("modal", {"is-active": props.isOpen })}>
           <div className="modal-background"></div>
          <div className="modal-card">
          <header className="modal-card-head">
          <p className="modal-card-title">Donor</p>
          <button className="delete" onClick={() => props.closeModal()}></button>
          </header>
          <section className="modal-card-body">

          <MyForm  sendNewPoint={props.sendNewPoint} point={props.point} latitude={props.latitude} longitude={props.longitude} isModalReadOnly={props.isModalReadOnly} />
          </section>
          </div>
           </div>);
};

class MyInputHoc extends React.Component {
  render() {
    return (
      <div className="field" >
        <label className="label">{this.props.label}</label>
        <input className="input" placeholder="Required" value={this.props.getValue()} onChange={(e) => this.props.setValue(e.target.value)} disabled={this.props.isFormDisabled()}/>
        {this.props.showError() ?
         <p className={classNames("help", "is-danger")}>{this.props.getErrorMessage()}</p> : <div />
        }
      </div>
    );
  }
};
const Input = HOC(MyInputHoc);

class MyHiddenInputHoc extends React.Component {
  render() {
    return (
        <input className="input" type="hidden" placeholder="Required" value={this.props.getValue()} onChange={(e) => this.props.setValue(e.target.value)}/>
    );
  }
};
const HiddenInput = HOC(MyHiddenInputHoc);

class MyForm extends Component {
  constructor(props){
    super(props);

    this.state = {
      canSubmit: true,
    };

    this.enableButton = this.enableButton.bind(this);
    this.disableButton = this.disableButton.bind(this);
    this.submit = this.submit.bind(this);
  }
  enableButton() {
    this.setState({
      canSubmit: true
    });
  }
  disableButton() {
    this.setState({
      canSubmit: false
    });
  }
  submit(model) {
    axios.get('//freegeoip.net/json/')
        .then((data)  => {
          model.ip = data.data.ip;
          console.log(model);
          this.props.sendNewPoint(model);
        })
  }

  render() {
    return (
      <Formsy.Form onValidSubmit={this.submit} onValid={this.enableButton} onInvalid={this.disableButton} disabled={this.props.isModalReadOnly}>

       <HiddenInput
         type='hidden'
         name='latitude'
         value={this.props.latitude}
       />

       <HiddenInput
         type='hidden'
         name='longitude'
         value={this.props.longitude}
       />

      <Input
      value={this.props.point ? this.props.point.firstname : 'defaultname'}
    name="firstname"
    label="First name"
    validationError="Invalid"
    required
      />
      <Input
      value={this.props.point ? this.props.point.lastname : 'defaultlastname'}
    name="lastname"
    label="Last name"
    validationError="Invalid"
    required
      />
      <Input
    name="contact"
      value={this.props.point ? this.props.point.contact : '111111'}
     label="Contact number"
      validationError="Invalid"
      validations={{
        matchRegexp: /(?:(\+?\d{1,3}) )?(?:([\(]?\d+[\)]?)[ -])?(\d{1,5}[\- ]?\d{1,5})/ //eslint-disable-line
      }}
    required
      />
      <Input
      value={this.props.point ? this.props.point.email : 'default@gmail.com'}
    name="email"
    label="Email"
    type="email"
      validationError="Invalid"
      validations="isEmail"
    required
      />
      <Input
      value={this.props.point ? this.props.point.address : 'address'}
    name="address"
    label="Address"
      validationError="Invalid"
    required
      />
      {
        this.props.isModalReadOnly ?
         <div />
        :
        (<button className="button is-primary" type="submit" disabled={!this.state.canSubmit}>Submit</button>)
      }
      </Formsy.Form>);
  }
}


export default PointModal;
