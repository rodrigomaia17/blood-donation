import { connect } from 'react-redux'
import BloodMap from '../components/BloodMap';
import mapDuck ,{ selectPoints, selectActualLocation, selectIsLoading, selectIsModalOpened, selectModalLatitude, selectModalLongitude, selectModalPoint, selectIsModalReadOnly } from '../ducks/mapDuck';

const mapStateToProps = state => {
  console.log(mapDuck);
  return {
    points: selectPoints(state.map),
    actualLocation: selectActualLocation(state.map),
    isLoading: selectIsLoading(state.map),
    isModalOpened: selectIsModalOpened(state.map),
    modalLatitude: selectModalLatitude(state.map),
    modalLongitude: selectModalLongitude(state.map),
    modalPoint: selectModalPoint(state.map),
    isModalReadOnly: selectIsModalReadOnly(state.map),
  }
}

const mapDispatchToProps = dispatch => {
  return {
    onLoadingFinished: () => dispatch(mapDuck.creators.finishLoading()),
    onViewUpdate: () => dispatch(mapDuck.creators.requestPoints()),
    openModal: (location) => dispatch(mapDuck.creators.openModal(location)),
    openModalReadOnly: (location) => dispatch(mapDuck.creators.openModalReadOnly(location)),
    closeModal: () => dispatch(mapDuck.creators.closeModal()),
    sendNewPoint: (model) => dispatch(mapDuck.creators.createPoint(model)),
  }
}

const BloodMapContainer = connect(
  mapStateToProps,
  mapDispatchToProps,
)(BloodMap);

export default BloodMapContainer

/* class BloodMapContainer extends Component {
 * 
 *   constructor(){
 *     super();
 *     this.state = {
 *       defaultMapPosition: null,
 *       points: [{latitude:-19.92, longitude:-43.93}],
 *       isModalOpened: false
 *     };
 * 
 *     this.openModal = this.openModal.bind(this);
 *   }
 * 
 *   componentDidMount(){
 *     const self = this;
 * 
 *     const socket = io.connect('http://localhost:3001');
 *     socket.on('connect', function(data) {
 *       socket.emit('join');
 *     });
 *     socket.on('init', (data) => {
 *       console.log('recebi ',data);
 *       const po = self.state.points.concat(data.points);
 *       setTimeout(() => {self.setState({points: po});}, 10000); // receber callback da volta
 *     });
 * 
 *   }
 * 
 *   openModal(){
 *     this.setState({isModalOpened: true});
 *   }
 * 
 *   const mapStateToProps = state => {
 *     return {
 *       todos: getVisibleTodos(state.todos, state.visibilityFilter)
 *     }
 *   }
 * 
 *   render() {
 * 
 *     return (
 *         <BloodMap
 *       points={this.state.points}
 *       defaultMapPosition={this.state.defaultMapPosition}
 *       onPointClicked={this.openModal}
 *         />
 *     );
 *   }
 * }*/
