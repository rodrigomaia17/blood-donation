import Duck from 'extensible-duck';
import { List } from 'immutable';
import Geolocator from 'geolocator';

const mapDuck = new Duck({
  namespace: 'blood', store: 'map',
  types: ['RECEIVE_POINTS', 'LOAD_FINISHED', 'REQUEST_POINTS', 'CHANGE_LOCATION', 'OPEN_MODAL', 'OPEN_MODAL_READONLY', 'CLOSE_MODAL', 'CREATE_POINT', 'RECEIVE_POINT'],
  initialState: {
    points: [],
    isLoading: true,
    actualLocation: null,
    isModalOpened: false,
    modalLatitude: 0,
    modalLongitude: 0,
    modalPoint: null,
    isModalReadOnly: false

  },
  reducer: (state, action, duck) => {
    switch(action.type) {
    case duck.types.RECEIVE_POINTS:
      if(!action.points.equals(state.points)){
        return {...state, points: List(action.points) };
      }
      return state;
    case duck.types.RECEIVE_POINT:
      return {...state, points: state.points.push(action.point)};
    case duck.types.LOAD_FINISHED:
      return {...state, isLoading: false };
    case duck.types.CHANGE_LOCATION:
      return {...state, actualLocation: action.location};
    case duck.types.OPEN_MODAL:
      return {...state, isModalOpened: true, isModalReadOnly: false, modalLatitude: action.location.latitude, modalLongitude: action.location.longitude, modalPoint: null};
    case duck.types.OPEN_MODAL_READONLY:
      const point = state.points.find( po => po.latitude === action.location.latitude);
      return {...state, isModalOpened: true, isModalReadOnly: true, modalPoint: point};
    case duck.types.CLOSE_MODAL:
      return {...state, isModalOpened: false};
    default: return state;
    }
  },
  creators: (duck) => ({
    requestPoints: () => (
      { type: duck.types.REQUEST_POINTS, socket: true, name: 'view-update' }),
    createPoint: (point) => (
      (dispatch) => {
        dispatch({ type: duck.types.CREATE_POINT, socket: true, name: 'create-point', data: point });
        dispatch(duck.creators.closeModal());
      }
    ),
    receivePoints: (points) => ({ type: duck.types.RECEIVE_POINTS, points: List(points) }),
    receivePoint: (point) => ({ type: duck.types.RECEIVE_POINT, point }),
    changeLocation: (location) => ({type: duck.types.CHANGE_LOCATION, location}),
    openModal: (location) => ({type: duck.types.OPEN_MODAL, location}),
    openModalReadOnly: (location) => ({type: duck.types.OPEN_MODAL_READONLY, location}),
    closeModal: () => ({type: duck.types.CLOSE_MODAL}),
    finishLoading: () => (
      (dispatch) => {
        dispatch({ type: duck.types.LOAD_FINISHED });

        const options = {
          enableHighAccuracy: false,
          timeout: 5000,
          maximumWait: 10000,     // max wait time for desired accuracy
          maximumAge: 60000,          // disable cache
          fallbackToIP: true,     // fallback to IP if Geolocation fails or rejected
        };

        return Geolocator.locate(options, (err,location) => {
          if (!err) {
            console.log("location ", location);
            const loc = [ location.coords.longitude, location.coords.latitude ];
            dispatch({type: duck.types.CHANGE_LOCATION, location: loc });
          } else {
            const defaultLocation = [ -44, -19 ];
            dispatch({type: duck.types.CHANGE_LOCATION, location: defaultLocation });
          }
        });
      }
    ),
  })
});

//selectors

export const selectPoints = (state) => state.points;
export const selectIsLoading = (state) => state.isLoading;
export const selectActualLocation = (state) => state.actualLocation;
export const selectIsModalOpened = (state) => state.isModalOpened;
export const selectModalLatitude = (state) => state.modalLatitude;;
export const selectModalLongitude = (state) => state.modalLongitude;
export const selectModalPoint = (state) => state.modalPoint;
export const selectIsModalReadOnly = (state) => state.isModalReadOnly;

export default mapDuck;
