import { createStore, applyMiddleware } from 'redux';
import thunk from 'redux-thunk';

import mapDuck,
{ selectPoints,
  selectIsLoading,
  selectActualLocation } from '../../src/ducks/mapDuck';

describe('map module test', () => {
  let store;
  let dispatch;

  beforeEach(() => {
    store = createStore(mapDuck.reducer, applyMiddleware(thunk));
  });


  test('test initial state', () => {
    const state = store.getState();
    expect(selectPoints(state)).toEqual([]);
    expect(selectIsLoading(state)).toEqual(true);
    expect(selectActualLocation(state)).toEqual(null);
  });

  test('test finish loading', () => {
    let state = store.getState();
    const action = mapDuck.creators.finishLoading();

    store.dispatch(action);

    state = store.getState();
    expect(selectIsLoading(state)).toEqual(false);
  });
  
  test('test load points', () => {
    let state = store.getState();
    const action = mapDuck.creators.finishLoading();

    store.dispatch(action);

    state = store.getState();
    expect(selectIsLoading(state)).toEqual(false);
  });

});

