import React from 'react';
import ReactDOM from 'react-dom';
import { Provider } from 'react-redux'
import { applyMiddleware, createStore } from 'redux'
import thunk from 'redux-thunk';
import logger from 'redux-logger'
import bloodApp from './reducers'
import BloodMapContainer from './containers/BloodMapContainer';
import registerServiceWorker from './registerServiceWorker';
import io from 'socket.io-client';
import './index.css';
import mapDuck from './ducks/mapDuck';

const socketMiddleware = socket => store => next => action => {
  if (action.socket) {
    socket.emit(action.name, action.data);
    console.log('action ', action.name, 'emitted');
  }
  return next(action);
}

const socket = io.connect('http://localhost:3001');


const store = createStore(
  bloodApp,
  applyMiddleware(
    socketMiddleware(socket),
    thunk,
    logger
  )
);

socket.on('points', (data) => {
  const action =  mapDuck.creators.receivePoints(data.points);
  store.dispatch(action);
});

socket.on('new-point', (data) => {
  console.log('novo-ponto', data);
  const action =  mapDuck.creators.receivePoint(data);
  store.dispatch(action);
});

ReactDOM.render(
  <Provider store={store}> 
    <BloodMapContainer />
  </Provider>,
  document.getElementById('root'));
registerServiceWorker();


