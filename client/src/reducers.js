import { combineReducers } from 'redux';
import mapDuck from './ducks/mapDuck';

export default combineReducers({ map: mapDuck.reducer });
