// server/app.js
import express from 'express';
import http from 'http';
import SocketIO from 'socket.io';

const generateServer = (models) => {

  const { Donor } = models;

  const morgan = require('morgan');
  const path = require('path');

  const app = express();
  const server = http.Server(app);
  const io = new SocketIO(server);

  app.use(morgan(':remote-addr - :remote-user [:date[clf]] ":method :url HTTP/:http-version" :status :res[content-length] :response-time ms'));


  app.use(express.static(path.resolve(__dirname, '..', 'client/build')));

  io.on('connection', function(client){
    client.on('view-update', (data) => {
      Donor.find().exec((err,donors) => {
        console.log(donors);
        client.emit('points', {points :donors});
      });
    });

    client.on('create-point', (data) => {
      console.log('I Got a New Point', data);
      const donor = new Donor({
        firstname: data.firstname,
        lastname: data.lastname,
        email: data.email,
        address: data.address,
        contactNumber: data.contact,
        latitude: data.latitude,
        longitude: data.longitude,
        ip: data.ip,
      });
      console.log('salvando ', donor);

      donor.save((err) => {
        if (err) {
          console.log(err);
        } else {
          console.log('saved');
          io.sockets.emit('new-point', donor);
        }
      });
    });

    console.log('a user connected');
  });

  return server;
}

// Always return the main index.html, so react-router render the route in the client
// app.get('*', (req, res) => {
//   res.sendFile(path.resolve(__dirname, '..', 'client/build', 'index.html'));
// });


module.exports = generateServer;
