import mongoose from 'mongoose';
import app from './app';

import models from './models';

mongoose.connect('mongodb://localhost/test');
const db = mongoose.connection;
db.on('error', console.error.bind(console, 'connection error:'));
db.once('open', () => {
  console.log('connected to mongo');



  const PORT = process.env.PORT || 3001;
  app(models).listen(PORT, () => {
    console.log(`App listening on port ${PORT}!`);
  });
});
