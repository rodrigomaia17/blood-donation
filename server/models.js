import mongoose from 'mongoose';


const donorSchema = mongoose.Schema({
  firstname: { type: String, required: true },
  lastname: { type: String, required: true },
  email: { type: String, required: true },
  address: { type: String, required: true },
  contactNumber: { type: String, required: true },
  latitude: { type: Number, required: true },
  longitude: { type: Number, required: true },
  ip: { type: String, required: true },
});

export const Donor = mongoose.model('Donor', donorSchema);
export default {Donor: Donor};
